#include "GA.h"



GA::GA(int W, int H, bool isProducer, int population_size, double crossing_over_rate, double mutation_rate, double space_limitation_rate)
{
	srand(time(NULL));
	GA::W = W;
	GA::H = H;
	GA::population_size = population_size;
	GA::isProducer= isProducer;
	GA::crossing_over_rate = crossing_over_rate;
	GA::mutation_rate = mutation_rate;
	GA::space_limitation_rate= space_limitation_rate;
	POPULATION = new population*[population_size];
	POOLING = new population*[population_size];
	for (int i = 0; i < population_size; i++) {
		if (isProducer) {
			POPULATION[i]=(new producerMarket(W,H));
		}
		else {
			POPULATION[i]=(new customerMarket(W, H));
		}
		POPULATION[i]->findFitness();
		cout << "Initialized:" << i + 1 << "/" << population_size <<" fitness:"<< POPULATION[i]->fitness<< endl;
	}
}
void GA::doPooling() {
	for (int i = 0; i < population_size; i++) {
		POOLING[i] = POPULATION[i];
	}
	sort(POOLING, POOLING+ population_size, [](population *a, population *b) {
		return a->fitness < b->fitness;
	});
	int  topop = 0.5*population_size,topush=0.7*population_size;
	pooling_size = population_size - topop;
	cur_p_size = topush;
	for (int i = 0; i < cur_p_size;i++) {
		POPULATION[i]=(POOLING[i]);
	}
	/*for (int i = cur_p_size; i <= population_size; i++) {
		delete POPULATION[i];
	}
	for (int i = pooling_size; i <= population_size; i++) {
		delete POOLING;
	}*/
	

}
void GA::crossingOver() {
	bool **temp_map = new bool*[H]; 
	for (int j = 0; j < H; j++) {
		temp_map[j] = new bool[W];
	}
	for (int t = cur_p_size; t < population_size;t++) {
		int fatherINDEX1 = random::randint(0, pooling_size - 1), motherINDEX1= random::randint(0, pooling_size - 1);
		int fatherINDEX2 = random::randint(0, pooling_size - 1), motherINDEX2 = random::randint(0, pooling_size - 1);
		while (motherINDEX1 == fatherINDEX1 || motherINDEX2 == fatherINDEX2 || motherINDEX1 == fatherINDEX2 || motherINDEX2 == fatherINDEX1) {
			fatherINDEX1 = random::randint(0, pooling_size - 1);
			motherINDEX1 = random::randint(0, pooling_size - 1);
			fatherINDEX2 = random::randint(0, pooling_size - 1);
			motherINDEX2 = random::randint(0, pooling_size - 1);
		}
		int fatherINDEX, motherINDEX;
		if (POOLING[fatherINDEX1]->fitness < POOLING[fatherINDEX2]->fitness) {
			fatherINDEX = fatherINDEX1;
		}
		else {
			fatherINDEX = fatherINDEX2;
		}

		if (POOLING[motherINDEX1]->fitness < POOLING[motherINDEX2]->fitness) {
			motherINDEX = motherINDEX1;
		}
		else {
			motherINDEX = motherINDEX2;
		}
		if (POOLING[fatherINDEX]->fitness > POOLING[motherINDEX]->fitness) {
			swap(fatherINDEX, motherINDEX);//father INDEx has more problibility to choose in crossing over phase;
		}
		allene *fatherDNA = (POOLING[fatherINDEX])->POS;
		allene *motherDNA = POOLING[motherINDEX]->POS, sonDNA[2000];
		
		//Copy father DNA to sonDNA
		for (int j = 0; j < 2000;j++) {
			allene new_temp= fatherDNA[j];
			sonDNA[j] = new_temp;
		}

		
		for (int j = 0; j < H; j++) {
			for (int k = 0; k < W; k++) {
				temp_map[j][k] = 0;
			}
		}
		

		for (int j = 0; j < 2000; j++) {
			temp_map[sonDNA[j].y][ sonDNA[j].x] = true;
		}

		for (int j = 0; j < 2000; j++) {
			//if the position is occupied, do nothing.
			if (temp_map[motherDNA[j].y][motherDNA[j].x]) {
				continue;
			}
			//random to choose between father or mother
			if (random::randint(0, 100) < crossing_over_rate * 100.0)
			{	
				temp_map[sonDNA[j].y][sonDNA[j].x] = false;
				sonDNA[j] = motherDNA[j];
				temp_map[motherDNA[j].y][motherDNA[j].x] = true;
				
			}
			else {
				temp_map[sonDNA[j].y][sonDNA[j].x] = false;
				sonDNA[j] = fatherDNA[j];
				temp_map[fatherDNA[j].y][fatherDNA[j].x] = true;
			}
		}

		if (isProducer) {
			POPULATION[t]=(new producerMarket(W, H, sonDNA));
			//cout << t << ":" << POPULATION[t]->findFitness()<<endl;
		}
		else {
			POPULATION[t] = (new customerMarket(W, H, sonDNA));
			//cout << t << ":" << POPULATION[t]->findFitness() << endl;
		}
	}
	for (int j = 0; j < H; j++) {
		delete[] temp_map[j];
	}
	delete[] temp_map;
}
void GA::mutation() {
	for (int i = 0; i < population_size; i++) {
		for (int j = 0; j <2000; j++) {
			if (random::randint(0, 100) >(double)mutation_rate*100.0) {
				continue;
			}
			int curTypr = POPULATION[i]->map[(POPULATION[i]->POS)[j].y][(POPULATION[i]->POS)[j].x];
			POPULATION[i]->map[(POPULATION[i]->POS)[j].y][(POPULATION[i]->POS)[j].x] = 0;
			int new_x = random::randint(0, W - 1), new_y = random::randint(0, H - 1);
			while (POPULATION[i]->map[new_y][new_x]!=0) {
				new_x = random::randint(0, W - 1);
				new_y = random::randint(0, H - 1);
			}
			POPULATION[i]->map[new_y][new_x] = curTypr;
			(POPULATION[i]->POS)[j].y = new_y;
			(POPULATION[i]->POS)[j].x = new_x;
		}
		
	}
}
void GA::spaceLimitation() {
	
	for (int i = 0; i < population_size; i++) {
		for (int j = 0; j <2000; j++) {
			RECT limit = POPULATION[i]->findLitmitedSpace( (POPULATION[i]->POS[j]).x, (POPULATION[i]->POS[j]).y );
			int curTypr = POPULATION[i]->map[(POPULATION[i]->POS)[j].y][(POPULATION[i]->POS)[j].x];
			POPULATION[i]->map[(POPULATION[i]->POS)[j].y][(POPULATION[i]->POS)[j].x] = 0;
			
			

			//int new_x = random::randint(limit.start_x, limit.end_x), new_y = random::randint(limit.start_y, limit.end_y);
			vector< pair<int, int> > toChoose;
			for (int start_x = limit.start_x; start_x <= limit.end_x; start_x++) {
				for (int start_y = limit.start_y; start_y <= limit.end_y; start_y++) {
					if (POPULATION[i]->map[start_y][start_x] == 0) {
						toChoose.push_back({ start_x ,start_y });
					}
					else {
						continue;
					}
				}
			}
			int toChooseIndex;
			//if there is no position to occupy, choose current position.
			if (toChoose.empty()) {
				int new_x = (POPULATION[i]->POS)[j].x;
				int new_y = (POPULATION[i]->POS)[j].y;
				POPULATION[i]->map[new_y][new_x] = curTypr;
				(POPULATION[i]->POS)[j].y = new_y;
				(POPULATION[i]->POS)[j].x = new_x;
			}
			else {
				//cout << "Pass" << endl;
				toChooseIndex = random::randint(0, toChoose.size() - 1);
				int new_x = toChoose[toChooseIndex].first;
				int new_y = toChoose[toChooseIndex].second;
				POPULATION[i]->map[new_y][new_x] = curTypr;
				(POPULATION[i]->POS)[j].y = new_y;
				(POPULATION[i]->POS)[j].x = new_x;
			}
			
		}

	}

}
pair<double, int> GA::findBestFitness() {
	double minFitness = 9999999999;
	int that_index=0;
	for (int i = 0; i < population_size; i++) {
		if (POPULATION[i]->fitness < minFitness) {
			that_index = i;
			minFitness = POPULATION[i]->fitness;
		}
	}
	
	return { minFitness, that_index };
}

GA::~GA()
{
}
