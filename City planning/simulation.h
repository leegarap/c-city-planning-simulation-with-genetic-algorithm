#pragma once
#include "GA.h"
#include <SDL/SDL.h>
class simulation
{
public:
	simulation(int W,int H, int W_GRID, int H_GRID,bool isProducer);
	void run();
	void evnt();
	void update();
	void draw();
	int W;
	int H;
	int W_GRID;
	int H_GRID;
	int W_S;
	int H_S;
	int N;
	int population_size;
	bool running;
	int bestFitnessIndex;
	SDL_Window *window;
	SDL_Renderer *renderer;
	GA *sim;
	pair<double, int> fit;
	FILE *f;
	~simulation();
};

