#pragma once
#include "const_struct.h"
class population
{
public:
	population(int W, int H);
	~population();
	population(int W, int H, allene *populations);
	double findDistant(int this_x, int this_y, int that_x, int that_y);
	virtual double findFitness()=0;
	virtual RECT findLitmitedSpace(int x, int y) = 0;
	int W;
	int H;
	const int P_TYPE[10] = { 1, 7, 14, 21, 28, 35, 42, 49, 56, 63 };
	const int P_RATIO[10] = { 683,341,228,171,137,114,97,85,76,68 };
	const int S = 2000;
	double fitness;
	int  **map;
	int P_POP[10];
	allene POS[2000];
	int POS_TYPE[11];
};

class producerMarket: public population {
public:
	producerMarket(int W, int H) ;
	~producerMarket();
	producerMarket(int W, int H, allene *populations);
	double findFitness() override;
	RECT findLitmitedSpace(int x, int y) override;
};

class customerMarket: public population {
public:
	customerMarket(int W, int H);
	~customerMarket();
	customerMarket(int W, int H,allene *populations);
	double findFitness() override;
	RECT findLitmitedSpace(int x, int y) override;
	int good_c_amount[10] = { 55, 155, 183, 189, 189, 186, 183, 179, 176, 173 };


};
