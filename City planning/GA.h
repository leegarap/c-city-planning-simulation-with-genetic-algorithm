#pragma once
#include "population.h"
class GA
{
public:
	GA(int W,int H,bool isProducer,int population_size,double crossing_over_rate,double mutation_rate,double space_limitation_rate);
	void doPooling();
	void crossingOver();
	void mutation();
	void spaceLimitation();
	pair<double,int> findBestFitness();
	int W, H,population_size, pooling_size,cur_p_size;
	bool isProducer;
	double crossing_over_rate, mutation_rate, space_limitation_rate;
	double bestFitness;
	population** POPULATION;
	population** POOLING;

	~GA();
};

