#include "population.h"



population::population(int W, int H)
{
	
	map=(int **)(malloc(sizeof(int *)*H));
	for (int j = 0; j < H; j++) {
		map[j] = (int *)(malloc(sizeof(int)*W));
	}
	population::W = W;
	population::H = H;
	for (int i = 0; i < 10; i++) {
		P_POP[i]=((int)((double)P_RATIO[i] / (double)S * (double)2000));
	}
	POS_TYPE[0]=0;
	int countPos = 0;
	for (int i = 0; i < 10; i++){
		for (int j = 0; j < P_POP[i]; j++) {
			
			int new_x = random::randint(0, W - 1), new_y = random::randint(0, H - 1);
			while (map[new_y][new_x]>0) {
				new_x = random::randint(0, W - 1);
				new_y = random::randint(0, H - 1);
			}
			map[new_y][new_x] = P_TYPE[i];
			POS[countPos]={ new_x,new_y,countPos };
			
			countPos++;
		}
		POS_TYPE[i+1]=(countPos);
		
	}
	//cout << "Initialized!" << endl;
}



population::population(int W, int H, allene *populations)
{
	map = (int **)(malloc(sizeof(int *)*H));
	for (int j = 0; j < H; j++) {
		map[j] = (int *)(malloc(sizeof(int)*W));
	}
	for (int i = 0; i < H; i++) {
		for (int j = 0; j < W; j++) {
			map[i][j]=0;
		}

	}
	population::W = W;
	population::H = H;
	for (int i = 0; i < 10; i++) {
		P_POP[i] = ((int)((double)P_RATIO[i] / (double)S * (double)2000));
	}
	POS_TYPE[0] = 0;
	int countPos = 0;
	for (int i = 0; i < 2000; i++){
		allene cop = populations[i];
	POS[i] = cop;
}
	int p = 0;
	for (int i = 0; i < 10;i++) {
		POS_TYPE[i+1]= POS_TYPE[i]+P_POP[i] ;
		for (int j = POS_TYPE[i]; j < POS_TYPE[i+1]; j++) {
			int new_x=POS[j].x, new_y= POS[j].y;
			map[new_y][new_x] = P_TYPE[i];
			p++;
		}
	}
}

double population::findDistant(int this_x, int this_y, int that_x, int that_y) {
	return (this_x - that_x)*(this_x - that_x) + (this_y - that_y)*(this_y - that_y);
}
/*----------------------------------Producer Market-------------------------------------------*/
producerMarket::producerMarket(int W, int H):population(W,H){


}
producerMarket::producerMarket(int W, int H, allene *populations) : population(W, H, populations) {


}
double producerMarket::findFitness() {
	double total_f = 0;
	for (int t = 0; t < 2000;t++) {

		int this_x = POS[t].x, this_y = POS[t].y;
		for (int i = 0; i < 10; i++) {
			double mini_dist = 999999999;
			for (int j = POS_TYPE[i]; j < POS_TYPE[i + 1]; j++) {

				int that_x = POS[j].x, that_y = POS[j].y;
				if  (that_x == this_x && that_y == this_y)continue;
				double distant = findDistant(this_x, this_y, that_x, that_y);

				mini_dist = min(distant, mini_dist);
				
			}
			total_f += mini_dist;
			
		}
	}
	producerMarket::fitness = total_f;
	
	return fitness;
}
RECT producerMarket::findLitmitedSpace(int x, int y) {
	position min_dist_pos[10];
	
	for (int i = 0; i < 10; i++) {
		double min_dist = 99999999999999;
		for (int j = POS_TYPE[i]; j < POS_TYPE[i + 1]; j++) {

			int that_x = POS[j].x, that_y = POS[j].y;
			if (that_x == x && that_y == y)continue;
			double distant = findDistant(x, y, that_x, that_y);
			if (distant < min_dist) {
				min_dist =distant;
				min_dist_pos[i] = make_pair(that_x, that_y);
			}
		}

	}
	int start_x = min_dist_pos[0].first, start_y = min_dist_pos[0].second, end_x = min_dist_pos[0].first, end_y = min_dist_pos[0].second;
	
	for (int i = 0; i < 10; i++) {
		start_x =min(min_dist_pos[i].first, start_x);
		start_y = min(min_dist_pos[i].second, start_y);
		end_x = max(min_dist_pos[i].first, end_x);
		end_y= max(min_dist_pos[i].second, end_y);
	}
	return { start_x,start_y,end_x,end_y };
}

/*----------------------------------Customer Market-------------------------------------------*/
customerMarket::customerMarket(int W, int H) :population(W, H) {


}
customerMarket::customerMarket(int W, int H, allene *populations) : population(W, H, populations) {


}
double customerMarket::findFitness() {
	double total_f = 0;
	for (int i = 0; i < 2000; i++) {
		int select = good_c_amount[(int)map[POS[i].y][POS[i].x] / 7];
		int this_x = POS[i].x, this_y = POS[i].y;
		allene temp[2000];
		for (int k = 0; k < 2000; k++) {
			temp[k] = POS[k];
		}
		sort(temp, temp+2000, [this_x, this_y, this](const allene &a, const allene &b) {
			return findDistant(this_x, this_y, a.x, a.y)<findDistant(this_x, this_y, b.x, b.y);
		});
		for (int j = 0; j < select; j++) {
			total_f += findDistant(this_x, this_y, temp[j].x, temp[j].y);
		}
	}
	customerMarket::fitness = total_f;
	return fitness;
}
RECT customerMarket::findLitmitedSpace(int x, int y) {
	allene temp[2000];
	for (int k = 0; k < 2000; k++) {
		temp[k] = POS[k];
	}
	int transaction = good_c_amount[(int)map[y][x] / 7];
	sort(temp, temp + 2000, [x, y, this](const allene &A, const allene &B) {
		return findDistant(x, y, A.x, A.y) < findDistant(x,y, B.x, B.y);
	});
	int start_x = temp[0].x, start_y = temp[0].y, end_x = temp[0].x, end_y = temp[0].y;
	for (int i = 0; i < transaction; i++) {
		start_x = min(temp[i].x, start_x);
		start_y = min(temp[i].y, start_y);
		end_x = max(temp[i].x, end_x);
		end_y = max(temp[i].y, end_y);
	}
	return {start_x,start_y,end_x,end_y };
}
population::~population() {
	for (int i = 0; i < H; i++) {
		delete[] map[i];
	}
	delete[] map;
}
producerMarket::~producerMarket() {
	for (int i = 0; i < H; i++) {
		delete[] map[i];
	}
	delete[] map;
}
customerMarket::~customerMarket() {
	for (int i = 0; i < H; i++) {
		delete[] map[i];
	}
	delete[] map;
}