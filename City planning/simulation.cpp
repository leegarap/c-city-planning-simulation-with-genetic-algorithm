#include "simulation.h"



simulation::simulation(int W, int H,int W_GRID,int H_GRID, bool isProducer)
{
	simulation::W = W;
	simulation::H = H;
	simulation::W_GRID = W_GRID;
	simulation::H_GRID = H_GRID;
	simulation::W_S = int(W / W_GRID);
	simulation::H_S = int(H / H_GRID);
	simulation::N = N;
	simulation::running =false;
	simulation::bestFitnessIndex = 0;
	population_size=50;
	if (isProducer) {
		f = fopen("D:/workspace/C/City planning GA/RES/P.csv", "w");
		sim = new GA(100, 100, true, population_size, 0.5, 0.05, 0.73);
	}
	else {
		f = fopen("D:/workspace/C/City planning GA/RES/C.csv", "w");
		sim = new GA(100, 100, false, population_size, 0.5, 0.05, 0.73);
	}
	SDL_Init(SDL_INIT_EVERYTHING);
	window = SDL_CreateWindow("CITY_PLANNING", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, W, H, 0);
	renderer = SDL_CreateRenderer(window, -1, 0);
	fprintf(f, "step,fitness\n");
}
void simulation::run() {
	
	running = true;
	int step = 1;
	while (running) {
		cout << "STEP:" << step << endl; 
		
		fit=sim->findBestFitness();
		
		cout << "Best Fitness:" << fit.first << endl;
		fprintf(f, "%d,%lf \n",step,fit.first);
		step++;
		draw();
		update();
		
		evnt();
	}
	fclose(f);
}
void simulation::evnt() {
	SDL_Event evt;
	SDL_PollEvent(&evt);
	switch (evt.type) {
	case SDL_QUIT:
		running = false;
		break;
	default:
		break;
	}
}
void simulation::update() {

	sim->doPooling();
	sim->crossingOver();
	sim->mutation();
	sim->spaceLimitation();
	for (int i = 0; i < population_size; i++) {
		(sim->POPULATION[i])->findFitness();
		//cout << "Fitness [" << i + 1 << "]:" << (sim->POPULATION[i])->fitness << endl;;
	}
}
void simulation::draw() {
	SDL_RenderClear(renderer);
	SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
	for (int i = 0; i < H_GRID; i++) {
		for (int j = 0; j < W_GRID; j++) {
			if ((sim->POPULATION[fit.second])->map[i][j] == 1) {
				SDL_SetRenderDrawColor(renderer, 255, 216, 230, 255);
			}
			else if((sim->POPULATION[fit.second])->map[i][j] == 7) {
				SDL_SetRenderDrawColor(renderer, 244, 172, 65, 255);
			}
			else if ((sim->POPULATION[fit.second])->map[i][j] == 14) {
				SDL_SetRenderDrawColor(renderer, 199, 244, 65, 255);
			}
			else if ((sim->POPULATION[fit.second])->map[i][j] == 21) {
				SDL_SetRenderDrawColor(renderer, 84, 232, 0, 255);
			}
			else if ((sim->POPULATION[fit.second])->map[i][j] == 28) {
				SDL_SetRenderDrawColor(renderer, 0, 232, 212, 255);
			}
			else if ((sim->POPULATION[fit.second])->map[i][j] == 35) {
				SDL_SetRenderDrawColor(renderer, 0, 143, 232, 255);
			}
			else if ((sim->POPULATION[fit.second])->map[i][j] == 42) {
				SDL_SetRenderDrawColor(renderer, 88, 0, 232, 255);
			}
			else if ((sim->POPULATION[fit.second])->map[i][j] == 49) {
				SDL_SetRenderDrawColor(renderer, 204, 0, 232, 255);
			}
			else if ((sim->POPULATION[fit.second])->map[i][j] == 56) {
				SDL_SetRenderDrawColor(renderer, 232, 0, 162, 255);
			}
			else if ((sim->POPULATION[fit.second])->map[i][j] == 63) {
				SDL_SetRenderDrawColor(renderer, 232, 0, 104, 255);
			}
			else {
				SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
			}
			SDL_Rect r;
			r.x = j * simulation::W_S;
			r.y= i* simulation::H_S;
			r.w = simulation::W_S;
			r.h = simulation::H_S;
			SDL_RenderFillRect(renderer, &r);
		}
	}
	SDL_RenderPresent(renderer);
}


simulation::~simulation()
{
	
	SDL_DestroyWindow(window);
	SDL_DestroyRenderer(renderer);
	SDL_Quit();
}
